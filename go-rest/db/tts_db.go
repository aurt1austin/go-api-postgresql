package db

import (
	"database/sql"
	"example/web-service-gin/my_type"
	"log"
)

func GetNameByID(db *sql.DB, reqID string) string {
	var resultName string
	userSql := "SELECT name FROM users WHERE id = $1"
	err := db.QueryRow(userSql, reqID).Scan(&resultName)
	if err != nil {
		log.Panicln("Failed to execute query: ", err)
	}
	return resultName
}

func GetUserByID(db *sql.DB, reqID string) my_type.User {
	var user my_type.User
	userSql := "SELECT id, email, password, name FROM users WHERE id = $1"
	err := db.QueryRow(userSql, reqID).Scan(&user.ID)
	if err != nil {
		log.Panicln("Failed to execute query: ", err)
	}
	return user
}
