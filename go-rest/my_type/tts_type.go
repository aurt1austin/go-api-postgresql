package my_type

type User struct {
	ID       int
	Email    string
	Password string
	Name     string
}
