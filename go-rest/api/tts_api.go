package api

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"

	"example/web-service-gin/db"
	"example/web-service-gin/my_type"
	"example/web-service-gin/util"

	_ "github.com/lib/pq"
)

func HandleRequest() {
	router := gin.Default()
	router.GET("/", homePage)
	router.GET("/test-con", testCon)
	router.GET("/get-name", getNameByID)
	router.POST("/post-name", postUserByID)
	router.Run("localhost:8080")
}

func homePage(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, gin.H{"message": "Homepage"})
}

func testCon(c *gin.Context) {
	log.Println("testCon")
	db := util.GetDBInstance()
	defer db.Close()

	var myUser my_type.User
	userSql := "SELECT id, email, password FROM users WHERE id = $1"
	err := db.QueryRow(userSql, 1).Scan(&myUser.ID, &myUser.Email, &myUser.Password)
	if err != nil {
		log.Fatal("Failed to execute query: ", err)
	}
	c.IndentedJSON(http.StatusOK, myUser)
}

func getNameByID(c *gin.Context) {
	reqID := c.Request.URL.Query().Get("id")
	conn := util.GetDBInstance()
	c.IndentedJSON(http.StatusOK,
		my_type.User{
			Name: db.GetNameByID(conn, reqID),
		})
	defer conn.Close()
}

func postUserByID(c *gin.Context) {
	var user my_type.User
	c.ShouldBindJSON(&user)
	conn := util.GetDBInstance()
	c.IndentedJSON(http.StatusOK,
		db.GetUserByID(conn, strconv.Itoa(user.ID)))
	defer conn.Close()
}
