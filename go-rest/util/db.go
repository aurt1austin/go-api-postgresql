package util

import (
	"database/sql"
	"fmt"
	"log"
)

const (
	host     = "localhost"
	port     = 15432
	user     = "admin"
	password = "cmm17"
	dbname   = "my_db"
)

func GetDBInstance() *sql.DB {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname))
	if err != nil {
		log.Fatal("Failed to open a DB connection: ", err)
	}
	return db
}
